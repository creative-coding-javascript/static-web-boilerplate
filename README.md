# Static Web Boilerplate (WIP)

## Getting started

Clone this repository and install dependency:

```
npm install
```

Run development server

```
npm start
```

Build for deploy:

```
npm run build
```